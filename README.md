# 爬虫抓取厦门本地宝限行信息

> http://xm.bendibao.com/traffic/2018116/54311.shtm

基于`Web Magic`框架，爬取厦门本地宝文章。

## 使用说明

1. 引入jar包

`out/artifacts/web_magic_jar/web-magic.jar`

2. 代码使用

通过无参构造一个启动类，执行start()方法。默认会在`C:\`生成文件，默认命名为：2021厦门限行最新消息（持续更新）。
```java
WebMagicApplication webMagicApplication = new WebMagicApplication();
webMagicApplication.start();
```

若需要修改爬取地址，以及存储地址
```java
WebMagicApplication webMagicApplication = new WebMagicApplication("http://xm.bendibao.com/traffic/2018116/54311.shtm","D:\\");
webMagicApplication.start();
```

或者修改：`com.tops.webmagic.constants.ConstantsField`类中的相应参数。

