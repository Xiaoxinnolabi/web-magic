package com.tops.webmagic.constants;

/**
 * @author yiping_wang
 */
public final class ConstantsField {
    /**
     * 爬取的CSS的文本
     */
    public static final String PAGE_CSS_CONTENT = "div.content";

    /**
     * 结束文本起始位置
     */
    public static final String END_CONTENT = "<div id=\"adInArticle\"></div>";

    /**
     * 厦门本地宝URL
     */
    public static final String XM_BDB_URL = "http://xm.bendibao.com/traffic/2018116/54311.shtm";

    /**
     * 默认文件保存地址
     */
    public static final String DEFAULT_SAVE_LOCATION = "C:\\";

    /**
     * 文件名
     */
    public static final String FILE_NAME = "2021厦门限行最新消息（持续更新）";

    /**
     * 文件后缀
     */
    public static final String FILE_POSTFIX = ".stm";

    /**
     * 页面访问状态码
     */
    public static final int PAGE_STATUS_200 = 200;

    /**
     * 正则匹配src
     */
    public static final String REX_IMG_SRC = "src\\s*=\\s*\"?(.*?)(\"|>|\\s+)";

    /**
     * 正则匹配文件后缀
     */
    public static final String REX_IMG_SUFFIX = "[\\w]+[\\.](jpeg|jpg|png)";

    /**
     * 处理图片XPATH
     */
    public static final String XPATH_IMG = "//*[@id=\"bo\"]/*/img";
}
