package com.tops.webmagic;

import com.tops.webmagic.constants.ConstantsField;
import com.tops.webmagic.dowloader.MyHttpClientDownloader;
import com.tops.webmagic.pipeline.MyFilePipeline;
import com.tops.webmagic.processor.XmPageProcessor;
import us.codecraft.webmagic.Spider;

/**
 * @author yiping_wang
 */
public class WebMagicApplication {

    private String url;
    private String saveUrl;

    /**
     * 无参构造使用默认值
     */
    public WebMagicApplication() {
        this.url = ConstantsField.XM_BDB_URL;
        this.saveUrl = ConstantsField.DEFAULT_SAVE_LOCATION;
    }

    public WebMagicApplication(String url, String saveUrl) {
        this.url = url;
        this.saveUrl = saveUrl;
    }

    public void start(){
        Spider.create(new XmPageProcessor()).addUrl(this.url).addPipeline(new MyFilePipeline(this.saveUrl)).setDownloader(new MyHttpClientDownloader()).run();
    }

    public static void main(String[] args) {
        WebMagicApplication webMagicApplication = new WebMagicApplication(ConstantsField.XM_BDB_URL,"C:\\");
//        WebMagicApplication webMagicApplication = new WebMagicApplication();

        webMagicApplication.start();
    }
}
